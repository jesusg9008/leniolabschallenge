package com.leniolabs.challenge.calculator.factory;

import com.leniolabs.challenge.calculator.FeeCalculatorIF;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

@Component
public class FeeCalculatorFactory {
    private final FeeCalculatorIF corporateAccountFeeCalculator;
    private final FeeCalculatorIF personalAccountFeeCalculator;

    /*En este factory hice uso del qualifier pero tambien pueden incluirse los beans de los calculators directamente, basandonos en el patron solid,
     si se agrega o se quita algun calculator solamente bastaria con modificar este factory.
    * */

    @Autowired
    public FeeCalculatorFactory(@Qualifier("corporateAccountFeeCalculator") FeeCalculatorIF corporateAccountFeeCalculator,
                                @Qualifier("personalAccountFeeCalculator") FeeCalculatorIF personalAccountFeeCalculator){
        this.corporateAccountFeeCalculator = corporateAccountFeeCalculator;
        this.personalAccountFeeCalculator = personalAccountFeeCalculator;
    }

    public FeeCalculatorIF getInstance(String accountType){
        if("personal".equalsIgnoreCase(accountType)){
            return this.personalAccountFeeCalculator;
        }
        else if("corporate".equalsIgnoreCase(accountType)){
            return this.corporateAccountFeeCalculator;
        }
        return null;
    }
}
